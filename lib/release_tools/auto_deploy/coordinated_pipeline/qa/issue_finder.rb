# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module CoordinatedPipeline
      module Qa
        class IssueFinder
          include ::SemanticLogger::Loggable

          def initialize(deploy_version)
            @deploy_version = deploy_version
          end

          def execute
            issue = search_qa_issue

            return unless issue

            logger.info('QA issue found', title: issue.title, url: issue.web_url)

            save_qa_issue_into_deploy_vars(issue.web_url)
          end

          private

          attr_reader :deploy_version

          def search_qa_issue
            project = Project::Release::Tasks

            options = {
              title: "#{deploy_version} QA Issue",
              labels: 'QA task'
            }

            GitlabClient.issues(project, options).first
          end

          def save_qa_issue_into_deploy_vars(issue_url)
            File.open('deploy_vars.env', 'a') do |deploy_vars|
              deploy_vars << "GITLAB_QA_ISSUE_URL='#{issue_url}'"
            end
          end
        end
      end
    end
  end
end
