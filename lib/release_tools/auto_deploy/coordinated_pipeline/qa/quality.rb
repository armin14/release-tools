# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module CoordinatedPipeline
      module Qa
        module Quality
          class MissingQaIssueURL < StandardError; end

          private

          def base_variables
            {
              DEPLOY_ENVIRONMENT: environment,
              DEPLOY_VERSION: deploy_version,
              GITLAB_QA_ISSUE_URL: qa_issue_url
            }
          end

          def environment
            project.environment
          end

          def qa_issue_url
            ENV['GITLAB_QA_ISSUE_URL']
          end
        end
      end
    end
  end
end
