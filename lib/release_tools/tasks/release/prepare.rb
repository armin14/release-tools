# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module Release
      class Prepare
        include ReleaseTools::Tasks::Helper

        attr_reader :version

        def initialize(version)
          @version = version
        end

        def execute
          issue = Issue.new(version)
          issue.execute

          if version.monthly?
            ReleaseTools::PickIntoLabel.create(version)
          else
            prepare_gitlab_patch_release
            prepare_omnibus_patch_release
            prepare_cng_patch_release

            blog_post_mr = prepare_patch_blog_post_merge_request(issue.release_issue)
            issue.release_issue.add_blog_mr_to_description(blog_post_mr.web_url)
          end
        end

        private

        def prepare_gitlab_patch_release
          merge_request = ReleaseTools::PreparationMergeRequest
            .new(project: ReleaseTools::Project::GitlabEe, version: version.to_ee)
          merge_request.create_branch!

          create_or_show_merge_request(merge_request)
        end

        def prepare_omnibus_patch_release
          merge_request = ReleaseTools::PreparationMergeRequest
            .new(project: ReleaseTools::Project::OmnibusGitlab, version: version.to_ce)
          merge_request.create_branch!

          create_or_show_merge_request(merge_request)
        end

        def prepare_cng_patch_release
          merge_request = ReleaseTools::PreparationMergeRequest
            .new(project: ReleaseTools::Project::CNGImage, version: version.to_ce)
          merge_request.create_branch!

          create_or_show_merge_request(merge_request)
        end

        def prepare_patch_blog_post_merge_request(patch_issue)
          create_or_show_merge_request(patch_issue.blog_post_merge_request)
        end
      end
    end
  end
end
