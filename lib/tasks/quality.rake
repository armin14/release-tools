# frozen_string_literal: true

namespace :quality do
  desc 'Sends a Slack notification about the status of qa'
  task :notify do
    ReleaseTools::AutoDeploy::CoordinatedPipeline::Qa::Notifier.new(
      pipeline_id: ENV['CI_PIPELINE_ID'],
      deploy_version: ENV['DEPLOY_VERSION'],
      environment: ENV['DEPLOY_ENVIRONMENT']
    ).execute
  end

  desc 'Find quality issue based on deploy_version'
  task :find_issue do
    ReleaseTools::AutoDeploy::CoordinatedPipeline::Qa::IssueFinder
      .new(ENV['DEPLOY_VERSION'])
      .execute
  end
end
