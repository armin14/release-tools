# frozen_string_literal: true

namespace :post_deploy_migrations do
  desc 'Sends a Slack notification about the status of post-deployment migrations'
  task :notify do
    ReleaseTools::AutoDeploy::CoordinatedPipeline::PostDeployMigrations::Notifier.new(
      pipeline_id: ENV['CI_PIPELINE_ID'],
      deploy_version: ENV['DEPLOY_VERSION'],
      environment: ENV['DEPLOY_ENVIRONMENT']
    ).execute
  end
end
