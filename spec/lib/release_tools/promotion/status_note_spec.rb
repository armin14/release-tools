# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Promotion::StatusNote do
  let(:version) { '42.0.202005220540-7c84ccdc806.59f00bb0515' }
  let(:ci_pipeline_url) { 'https://example.com/pipeline' }
  let(:status) { double('status', fine?: true, to_issue_body: 'a collection of check results') }
  let(:override_reason) { 'false' }
  let(:services_status) { [] }

  subject(:note) do
    described_class.new(
      status: status,
      package_version: version,
      override_reason: override_reason
    )
  end

  describe '#release_manager' do
    it 'reads the username from RELEASE_MANAGER env variable' do
      definitions = double('Definitions')
      allow(ReleaseTools::ReleaseManagers::Definitions)
        .to receive(:new)
        .and_return(definitions)

      expect(definitions).to receive(:find_user)
        .with('ops_username', instance: :ops)
        .and_return(double('User', production: 'username'))

      ClimateControl.modify(RELEASE_MANAGER: 'ops_username') do
        expect(note.release_manager).to eq('@username')
      end
    end

    it 'tracks unknown release managers' do
      ClimateControl.modify(RELEASE_MANAGER: 'ops-unknown') do
        expect(note.release_manager)
          .to eq(":warning: Unknown release manager! OPS username ops-unknown")
      end
    end
  end

  describe '#body' do
    before do
      allow(note).to receive(:release_manager).and_return('@liz.lemon')
    end

    around do |ex|
      ClimateControl.modify(CI_PIPELINE_URL: ci_pipeline_url, &ex)
    end

    it 'links the pipeline' do
      expect(note.body).to include("[deployer pipeline](#{ci_pipeline_url})")
    end

    it 'includes the generated report' do
      expect(note.body).to include(status.to_issue_body)
    end

    context 'when the pipeline url is nil' do
      let(:ci_pipeline_url) { nil }

      it 'does not link the pipeline' do
        expect(note.body).not_to include('deployer pipeline')
      end
    end

    it 'pings the release manager' do
      expect(note.body).to include("@liz.lemon")
    end

    it 'includes the package version' do
      expect(note.body).to include("started a production deployment for package `#{version}`")
    end

    it "doesn't include the overriding reason when not override_status is false" do
      expect(note.body).not_to include(override_reason)
    end

    context 'when overriding the status' do
      let(:override_reason) { 'a reason to override the status' }

      it 'warns the status was overridden' do
        expect(note.body).to include(":warning: Status check overridden!")
        expect(note.body).to include(override_reason)
      end
    end
  end
end
