# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Rollback::Presenter do
  include RollbackHelper

  let(:post_deploy_migration) do
    {
      'new_path' => 'db/post_migrate/20211030_new_post_deploy_migration.rb'
    }
  end

  describe '#present' do
    let(:deployment) { rollback_upcoming_deployment_stub }

    it 'includes current and target package names' do
      comparison = rollback_stub_comparison
      current = comparison.current
      target = comparison.target

      instance = described_class.new(comparison, deployment)
      present = instance.present

      expect(present).to include("*Current:* `#{current}`\n")
      expect(present).to include("*Target:* `#{target}`\n")
    end

    it 'includes a comparison link' do
      comparison = rollback_stub_comparison(web_url: '<compare_url>')

      instance = described_class.new(comparison, deployment)

      expect(instance.present).to include("*Comparison:* <compare_url>\n")
    end

    it 'includes a warning for a timeout' do
      comparison = rollback_stub_comparison(timeout?: true)

      instance = described_class.new(comparison, deployment)

      expect(instance.present).to include(":warning: Comparison timed out\n")
    end

    it 'includes a warning for an empty compare' do
      comparison = rollback_stub_comparison(empty?: true)

      instance = described_class.new(comparison, deployment)

      expect(instance.present).to include(":warning: Comparison was empty\n")
    end

    it 'includes a count of post-deploy migrations' do
      comparison = rollback_stub_comparison(post_deploy_migrations: [post_deploy_migration])

      instance = described_class.new(comparison, deployment)
      lines = instance.present.flatten.collect(&:to_s)

      expect(lines).to include(":new: 1 post-deploy migrations\n")
      expect(lines).not_to include(a_string_matching(/\d+ migrations/))
    end

    it 'includes links to new migrations' do
      comparison = rollback_stub_comparison(post_deploy_migrations: [post_deploy_migration])

      instance = described_class.new(comparison, deployment)
      lines = instance.present.flatten.collect(&:to_s)

      expect(lines).to include(a_string_matching(/\[`20211030_new_post_deploy_migration.rb`\]\(.+\)/))
    end
  end

  describe '#header' do
    context 'when rollback is available' do
      it 'states the rollback is available' do
        deployment = rollback_upcoming_deployment_stub
        comparison = rollback_stub_comparison(safe?: true)

        instance = described_class.new(comparison, deployment)

        expect(instance.header).to eq(':large_green_circle: Rollback available')
      end
    end

    context 'when rollback is not available' do
      context 'with post-deploy migrations' do
        it 'states rollback is not available' do
          deployment = rollback_upcoming_deployment_stub
          comparison = rollback_stub_comparison(safe?: false, post_deploy_migrations: [post_deploy_migration])

          instance = described_class.new(comparison, deployment)

          expect(instance.header).to eq(':red_circle: Rollback unavailable - Post-deployment migrations included')
        end
      end

      context 'with a running deployment' do
        it 'states rollback is not available' do
          deployment = rollback_upcoming_deployment_stub(any?: true)
          comparison = rollback_stub_comparison(safe?: true)

          instance = described_class.new(comparison, deployment)

          expect(instance.header).to eq(':red_circle: Rollback unavailable - A deployment is in progress')
        end
      end

      context 'with post-deploy migrations and a running deployment' do
        it 'states rollback is not available' do
          deployment = rollback_upcoming_deployment_stub(any?: true)
          comparison = rollback_stub_comparison(safe?: false, post_deploy_migrations: [post_deploy_migration])

          instance = described_class.new(comparison, deployment)

          expect(instance.header).to eq(':red_circle: Rollback unavailable ')
        end
      end
    end
  end

  describe '#unavailable_rollback_multireason?' do
    it 'returns true with multiple reasons' do
      deployment = rollback_upcoming_deployment_stub(any?: true)
      comparison = rollback_stub_comparison(safe?: false, post_deploy_migrations: [post_deploy_migration])

      instance = described_class.new(comparison, deployment)

      expect(instance).to be_unavailable_rollback_multireason
    end

    it 'returns false with a single reason' do
      deployment = rollback_upcoming_deployment_stub(any?: false)
      comparison = rollback_stub_comparison(safe?: true)

      instance = described_class.new(comparison, deployment)

      expect(instance).not_to be_unavailable_rollback_multireason
    end
  end

  describe '#rollback_unavailable_reasons_block' do
    it 'returns the unavailable reasons for rollback' do
      deployment = rollback_upcoming_deployment_stub(any?: true)
      comparison = rollback_stub_comparison(safe?: false, post_deploy_migrations: [post_deploy_migration])

      instance = described_class.new(comparison, deployment)

      expect(instance.rollback_unavailable_reasons_block)
        .to eq("- A deployment is in progress\n- Post-deployment migrations included")
    end
  end
end
