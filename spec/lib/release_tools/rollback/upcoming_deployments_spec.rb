# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Rollback::UpcomingDeployments do
  let!(:fake_client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:environment) { 'gprd' }

  subject(:upcoming_deployments) do
    described_class.new(environment: environment)
  end

  describe '#running?' do
    let(:fake_response) { double(:response) }

    before do
      allow(fake_client)
        .to receive(:deployments)
        .and_return(fake_response)
    end

    context 'with a running deployment' do
      let(:deployment) { create(:deployment, :running) }

      before do
        allow(fake_response)
          .to receive(:paginate_with_limit)
          .and_return([deployment])
      end

      it 'returns true' do
        expect(upcoming_deployments).to be_any
      end
    end

    context 'with no running deployment' do
      it 'returns false' do
        allow(fake_response)
          .to receive(:paginate_with_limit)
          .and_return([])

        expect(upcoming_deployments).not_to be_any
      end
    end

    context 'with an invalid environment' do
      let(:environment) { 'foo' }

      it 'returns false' do
        expect(upcoming_deployments).not_to be_any
      end
    end
  end
end
