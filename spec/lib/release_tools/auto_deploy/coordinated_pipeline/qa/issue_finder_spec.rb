# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::CoordinatedPipeline::Qa::IssueFinder do
  let(:deploy_version) { '14.6.202112010920-d8ee40bb7b5.067fb3321f5' }
  let(:project) { ReleaseTools::Project::Release::Tasks }
  let(:fake_client) { stub_const('ReleaseTools::GitlabClient', spy) }

  subject(:finder) { described_class.new(deploy_version) }

  after do
    FileUtils.rm_r('deploy_vars.env') if File.exist?('deploy_vars.env')
  end

  describe '#execute' do
    context 'with QA issue' do
      let(:qa_issue) { create(:issue, title: 'QA task') }

      before do
        allow(fake_client)
          .to receive(:issues)
          .and_return([qa_issue])
      end

      it 'finds the QA issue' do
        options = {
          title: "#{deploy_version} QA Issue",
          labels: 'QA task'
        }

        expect(fake_client)
          .to receive(:issues)
          .with(project, options)

        finder.execute
      end

      it 'updates deploy_vars with it' do
        finder.execute

        expect(File.read('deploy_vars.env'))
          .to eq("GITLAB_QA_ISSUE_URL='#{qa_issue.web_url}'")
      end
    end

    context 'with no QA issue' do
      it 'does nothing' do
        File.open('deploy_vars.env', 'w') {} # Simulating an existing file

        allow(fake_client)
          .to receive(:issues)
          .and_return([])

        finder.execute

        expect(File.read('deploy_vars.env')).to eq('')
      end
    end
  end
end
