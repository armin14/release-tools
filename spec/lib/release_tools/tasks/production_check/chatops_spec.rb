# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

describe ReleaseTools::Tasks::ProductionCheck::Chatops do
  let!(:fake_webhook) do
    stub_const(
      'ReleaseTools::Slack::ChatopsNotification',
      double(fire_hook: true)
    )
  end

  def status_stub(messages = {})
    instance_double(
      'ReleaseTools::Promotion::ProductionStatus',
      {
        to_slack_blocks: ['blocks'],
        fine?: true
      }.merge(messages)
    )
  end

  describe '#execute' do
    around do |ex|
      ClimateControl.modify(CHAT_CHANNEL: 'channel', &ex)
    end

    it 'notifies the specified Slack channel' do
      instance = described_class.new
      status = status_stub(fine?: true)

      stub_const('ReleaseTools::Promotion::ProductionStatus', double(new: status))

      without_dry_run do
        instance.execute
      end

      expect(fake_webhook).to have_received(:fire_hook)
        .with(channel: 'channel', blocks: ['blocks'])
    end

    it 'includes active deployment check by default' do
      instance = described_class.new
      status = class_double('ReleaseTools::Promotion::ProductionStatus')

      expect(status).to receive(:new)
        .with(:canary_up, :active_deployments)
        .and_return(spy)

      stub_const('ReleaseTools::Promotion::ProductionStatus', status)

      without_dry_run do
        instance.execute
      end
    end

    it 'skips active deployment check when SKIP_DEPLOYMENT_CHECK is set' do
      instance = described_class.new
      status = class_double('ReleaseTools::Promotion::ProductionStatus')

      expect(status).to receive(:new)
        .with(:canary_up)
        .and_return(spy)

      stub_const('ReleaseTools::Promotion::ProductionStatus', status)

      ClimateControl.modify(SKIP_DEPLOYMENT_CHECK: 'true') do
        without_dry_run do
          instance.execute
        end
      end
    end

    it 'raises UnsafeProductionError when requested for an unsafe status' do
      instance = described_class.new
      status = status_stub(fine?: false)

      stub_const('ReleaseTools::Promotion::ProductionStatus', double(new: status))

      ClimateControl.modify(FAIL_IF_NOT_SAFE: 'true') do
        expect { instance.execute }.to raise_error(described_class::UnsafeProductionError)
      end
    end
  end
end
